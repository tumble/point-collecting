module.exports = {
	apps: [{
		name: "point-collecting",
		script: 'npm',
		args: 'start',
		//interpreter: '/bin/bash',
		env: {
			NODE_ENV: "production",
			PORT: 4001,
		}
	}]
};
